# Vibhesh Patneedi

import unittest

def binary_search(arr, x):
    low, high = 0, len(arr) - 1

    while (low <= high):
        mid = (low + high)//2

        if arr[mid] == x:
            return mid
        elif arr[mid] < x:
            low = mid + 1  
        else:
            high = mid - 1 
    return -1


class TestBinarySearch(unittest.TestCase):

    def test_empty_arr(self):
        arr = []
        self.assertEqual(binary_search(arr, 3), -1)

    def test_single_element_arr(self):
        arr = [3]
        self.assertEqual(binary_search(arr, 3), 0)

    def test_general_arr(self):
        arr = [1, 2, 3, 4, 5]
        self.assertEqual(binary_search(arr, 3), 2)

    def test_odd_size_arr(self):
        arr = [1, 2, 3, 4, 5, 6, 7]
        self.assertEqual(binary_search(arr, 4), 3)

    def test_even_size_arr(self):
        arr = [1, 2, 3, 4, 5, 6]
        self.assertEqual(binary_search(arr, 3), 2)

    def test_first_element(self):
        arr = [1, 2, 3, 4, 5]
        self.assertEqual(binary_search(arr, 1), 0)

    def test_last_element(self):
        arr = [1, 2, 3, 4, 5]
        self.assertEqual(binary_search(arr, 5), 4)

    def test_element_not_present(self):
        arr = [1, 2, 3, 4, 5]
        self.assertEqual(binary_search(arr, 6), -1)

    def test_repeated_element(self):
        arr = [1, 2, 3, 3, 4, 5]
        self.assertEqual(binary_search(arr, 3), 2)

if __name__ == '__main__':
    unittest.main()
